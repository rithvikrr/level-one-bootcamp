//Write a program to find the volume of a tromboloid using one function
#include <stdio.h>

float volofT(int, int,int);

int main()
{
    int h,d,b;
    float vol;
    printf("Enter the h ");
    scanf("%d", &h);

    printf("Enter the b ");
    scanf("%d", &b);
    
    printf("Enter the d : ");
    scanf("%d", &d);

    vol = volofT(h,b,d);

    printf("Volume of a tromboloid %f", vol);

    return 0;
}

float volofT(int h, int b,int d)
{
    return (1/3.0 * ((h * d *b) + ( d / b)));
}