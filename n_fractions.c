//WAP to find the sum of n fractions.
#include <stdio.h>

struct fract
{
    int num;
    int dem;
} ;


struct fract input()
{
    struct fract x;

    printf("Enter the numerator for Fraction :" );
    scanf("%d",&x.num);


    printf("Enter the denominator for Fraction : ");
    scanf("%d",&x.dem);
    return x;
}


int gcdcalculation(int a, int b)  
{
    while(a!=0)
    {
        int t=a;
        a=b%a;
        b=t;
    }
   return b; 
}  







struct fract add(struct fract n1, struct fract n2)
{
    struct fract ans;
    int x,y,gcd;
    
    x= ( (n1.num*n2.dem) + (n1.dem*n2.num) );       //numerator
    y= (n1.dem*n2.dem);                    //denominator

    gcd = gcdcalculation(x,y);

    ans.num=x/gcd;
    ans.dem=y/gcd;
    
    return ans;
}


struct fract nFracts_Sum(struct fract nf[],int n)
{
    struct fract ans;
    
    ans.num=nf[0].num;
    ans.dem=nf[0].dem;
    
    for(int i=1;i<n;i++)
    {
        ans=add(ans,nf[i]);
    }
    return ans;
}


void  print (struct fract ans, struct fract nf[] ,int n )
{
     printf("The sum of the fractions : %d/%d",nf[0].num,nf[0].dem);
    for(int i=1;i<n;i++)
    {
        printf(" + %d/%d",nf[i].num,nf[i].dem);
    }
    printf(" = %d/%d\n",ans.num,ans.dem);
}

int main()
{

struct fract ans;

int n;
printf("Enter N :" );
scanf("%d",&n);


struct fract nfrac[n];

for(int i=0;i<n;i++)
    {
        printf("Fraction %d :\n",i+1 );
        nfrac[i]=input();
    }

ans=nFracts_Sum(nfrac,n);

print (ans,nfrac,n);

return 0;
}